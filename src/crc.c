#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "crc32.h"

int main(int argc, char** argv)
{
    FILE* fp;
    char* filename = argv[1];
    size_t size;
    uint8_t* filedata;
    size_t numread;
    uint32_t filecrc;

    if (argc != 2)
    {
        printf("\nUsage: crc <file>\n\n");
        return 0;
    }

    fp = fopen(filename,"rb");
    if (fp == NULL)
    {
        printf("Cannot open file %s\n",filename);
        return -1;
    }
    fseek(fp, 0, SEEK_END); // seek to end of file
    size = ftell(fp); // get current file pointer
    fseek(fp, 0, SEEK_SET); // seek back to beginning of file

    filedata = (uint8_t*)malloc(size);
    if (filedata == NULL)
    {
        printf("Cannot allocate memory for file\n");
        goto error;
    }

    numread = fread(filedata,1,size,fp);
    if (numread != (size_t)size)
    {
        printf("Could not read entire file!\n");
        goto error;
    }

    filecrc = ltree_crc32_init();
    for(int i = 0; i<(int)numread; i++)
    {
        filecrc = ltree_crc32_add(filecrc,filedata[i]);
    }

    printf("GSM Communicator V2+ firmware check (for Zephyr RTOS based versions)\n");
    printf("File: %s\n",filename);
    printf("Length: %lu ",size);
    if (size == (1024*960))
    {
        printf("[OK]\n");
    }
    else
    {
        printf("Warning! Incorrect lenght!!\n");
    }
    
    printf("CRC: %08X\n",filecrc);

error:
    if (fp != NULL)
    {
        fclose(fp);
    }
    if (filedata != NULL)
    {
        free(filedata);
    }

    return 0;
}