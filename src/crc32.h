#ifndef CRC32_H
#define CRC32_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

unsigned int ltree_crc32_init(void);
unsigned int ltree_crc32_add(unsigned int crc, uint8_t val);
unsigned int ltree_crc32_sz(char *buf, int size);



#ifdef __cplusplus
}
#endif



#endif // CRC32_H